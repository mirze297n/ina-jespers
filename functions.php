<?php
use Eppeg\inajaspers\App;

define('inajaspers_Template_Path',get_template_directory());
define('inajaspers_Template_Url',get_template_directory_uri());

spl_autoload_register( function ( $class_name ) {
    if ( false !== strpos( $class_name, 'Eppeg\\inajaspers\\' ) ) {
        $class_file = str_replace( 'Eppeg\\inajaspers\\', '', $class_name ) . '.class.php';
        $class_file = str_replace( '\\', DIRECTORY_SEPARATOR, $class_file );
        $class_file_path =  inajaspers_Template_Path . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . $class_file;
        error_log($class_file_path);
        if (file_exists($class_file_path)) {
            require_once( $class_file_path );
        }
    }
});

App::run();
