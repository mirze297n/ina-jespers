<?php
use Eppeg\inajaspers\Utils;
get_header();

Utils::renderTemplate('section1');
Utils::renderTemplate('section2');
Utils::renderTemplate('section3');
Utils::renderTemplate('section4');

get_footer();