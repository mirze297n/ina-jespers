<?php

use \Eppeg\inajaspers\Utils;
$title = get_field('home_page_contact_title');
$text = get_field('home_page_contact_text');
$company_name =get_field('contact_company_name');
$address = get_field('contact_address');
$mail = get_field('contact_company_mail');
$phone = get_field('contact_company_phone');
$link = get_field('contact_button_link');
?>
<section class="mb-8 contact" id="contact">
    <div class="container">
        <div class="content row">
            <div class="contact-info text-right col-50">
                <h1 class="fs-8 mb-2 mt-0"><?php echo $title;?></h1>
                <p ><?php echo $text;?></p>
                <div class="mt-4">
                    <p class=" m-0"><?php echo $company_name;?></p>
                    <p class=" m-0"> <?php echo $address;?></p>
                    <p class=" m-0"> <?php echo $mail;?></p>
                    <p class=" m-0"><?php echo $phone;?></p>
                </div>

                <a href="<?php echo $link;?>" class="md-btn mt-3 fs-1">
                    Contacteer ons
                </a>
            </div>
            <div class="contact-map ml-5 col-50">
<!--                <iframe-->
<!--                        width="100%"-->
<!--                        height="100%"-->
<!--                        style="border:0"-->
<!--                        loading="lazy"-->
<!--                        allowfullscreen-->
<!--                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2512.334896362183!2d5.516963658975547!3d50.97300013396162!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c0d93659bcdb6d%3A0xd217cfda92fca786!2sSint-Lodewijkstraat%2062%2C%203600%20Genk%2C%20Bel%C3%A7ika!5e0!3m2!1sen!2s!4v1633707556009!5m2!1snl!2s">-->
<!--                </iframe>-->
                <div id="map"></div>
            </div>
        </div>
    </div>
</section>
