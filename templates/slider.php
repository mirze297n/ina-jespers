<?php

use Eppeg\inajaspers\App;

$slides = App::getSliderFields();
?>

<div class="row slider-row">
    <div class="col-30 ">
        <div class="section-text">

            <p>Donec velit enim, ultrices id nulla quis, vehicula pharetra ligula. Sed porttitor orci vitae gravida
                varius. Quisque diam nunc, hendrerit quis bibendum a, varius nec nulla. Nunc vitae metus laoreet,
                ultrices nisi quis, varius enim. Aliquam malesuada eros vitae est aliquam suscipit. Vestibulum ac diam
                lacinia, cursus orci vel, elementum elit.

                <br><br>- Zaha Hadid</p>
        </div>
    </div>
    <div class="col-70">
        <?php foreach ($slides as $slide) : ?>
            <div class="slider-item">
                <div class="section-image">
                    <img src="<?php echo get_the_post_thumbnail_url($slide->ID, 'full') ?>">
                </div>
            </div>
        <?php
        endforeach;
        ?>
        <div class="pager">

        </div>
    </div>
</div>







