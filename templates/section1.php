<?php

use \Eppeg\inajaspers\Utils;

?>
<section class="mb-8">

    <div class="slider text-right">
        <div class="slides-container">
            <div class="slide-items">
                <?php
                Utils::renderTemplate('slider');
                ?>

            </div>

<!--            <div class="arrows">-->
<!--                <a class="prev" href="#"><</i></a>-->
<!--                <a class="next" href="#">></a>-->
<!--            </div>-->
            <div class="slider-loader">
                <div class="loader-wrapper">
                    <div class="lds-roller">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>

            </div>
        </div>


    </div>

</section>
