<?php
use \Eppeg\inajaspers\Utils;
$image = get_field('home_page_studio_image');
$title = get_field('home_page_title');
$text_title = get_field('home_page_text_title');
$content = get_field('home_page_content');
$link =get_field('home_page_button_link');
?>
<section class="bg-yellow mb-8 studio " id="studio">
    <div class="container pd-v-lg-9 ">
        <div class="row align-center">
            <div class="col-60">
                <div class="studio-image">
                    <img src="<?php echo $image;?>" >
                </div>
            </div>
            <div class="col-40">
                <div class="content-text">
                    <h1 class="lh-1"><?php echo $title ;?></h1>
                    <div class="fw-5 pdl-3 visie-text">
                        <h2 class="fs-3 m-0"><?php echo $text_title ;?></h2>
                        <p class="pdl-3" >  <?php echo $content;?></p>
                        <div class="content-text__info-button-wrapper pd-h-lg-3 pdt-1">
                            <a href="<?php echo $link;?>" class="md-btn fs-1">Contacteer ons</a>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</section>
