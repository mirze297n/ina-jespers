<?php
$images = acf_photo_gallery('projecten_gallery', get_the_ID());
$last_image = array_key_last($images);

foreach ($images as $key => $image) {
    $full_image_url = $image['full_image_url'];
    if ($key == 0 || $key == 3) {
        echo '<div class="image-wrapper">
                                   <a href="' . $full_image_url . '" data-lightbox="roadtrip">  <img class="full-width" src="' . $full_image_url . '"></a>
                                 </div>';
        continue;
    } elseif ($key == 1 || $key == 2) {
        if ($key == 1) {
            echo '<div class="row col-gap-2 pd-v-lg-3">';
        }
        echo '<div class="image-wrapper col-50">
                        <a href="' . $full_image_url . '" data-lightbox="roadtrip">            <img class="full-width" src="' . $full_image_url . '"></a>
                                  </div>';
        if ($key == 2) {
            echo '</div>';
        }
        continue;
    } elseif ($key == 4) {
        echo '<div class="post-image-slider row col-gap-1 pd-v-lg-1">';
    }
    echo ' <div class="col-20 "><a href="' . $full_image_url . '" data-lightbox="roadtrip"> <img src="' . $full_image_url . '"></a></div>';
    if ($key == $last_image) {
        echo '</div>';
    }
}
