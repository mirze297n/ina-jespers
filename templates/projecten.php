<?php

use Eppeg\inajaspers\Utils;
use Eppeg\inajaspers\App;

$terms = get_terms([
    'taxonomy' => 'projecten_categories',
    'hide_empty' => true,
]);
?>


<div class="projecten-list fs-1 fw-5 col-30">
    <ul class="pd-h-lg-6 m-0">
        <?php
        foreach ($terms as $key => $term):?>
            <li class="projecten-list-item  pd-1 fss-5 pointer hvr-yllw fw-6 pdb-2 <?php if ($key == 0) {
                echo 'active';
            } ?>" data-content="<?php echo strtolower($term->name); ?>-images">
                <?php echo $term->name ?>
            </li>
        <?php
        endforeach;
        ?>
    </ul>
</div>
<div class="projecten-images col-70">

    <?php
    foreach ($terms as $key => $term):?>
    <div class="<?php echo strtolower($term->name); ?>-images grid-div <?php if ($key == 0) {
        echo 'active-grid';
    } ?>">
        <?php
                $query = App::getPostsbyCat($term->slug);
                    foreach ($query->posts as $projecten): ?>
                        <div class="image-wrapper">
                            <a class="c-yllw td-none" href="<?php echo $projecten->guid;?>"> <img src="<?php echo get_the_post_thumbnail_url($projecten->ID, 'full') ?>"></a>
                            <div class="bottom">
                                <div class="img-text">
                                    <h2 class="m-0 c-yllw fss-6"><a class="c-yllw td-none" href="<?php echo $projecten->guid;?>"> <?php echo $projecten->post_title?></a></h2>
                                    <p class="m-0 fs-1"><?php echo get_field('projecten_category', $projecten->ID)?>, <?php echo get_field('projecten_year', $projecten->ID)?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;
                ?>
        </div>
        <?php
    endforeach;
    ?>


</div>
