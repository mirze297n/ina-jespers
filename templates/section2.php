<?php
use \Eppeg\inajaspers\Utils;
?>
<section class="mb-8 projecten" id="projecten">
    <div class="container">
        <div class="projecten-header header pd-v-lg-2 fs-5">
            <h1 class="m-0">Projecten</h1>
        </div>
        <div class="content row">
            <?php
             Utils::renderTemplate('projecten');
            ?>
        </div>
    </div>
</section>