<?php

use Eppeg\inajaspers\Utils;

$title = get_field('projecten_name');
$year = get_field('projecten_year');
$category = get_field('projecten_category');

get_header();
//echo '<pre>';
//print_r($images);
//echo '</pre>';
?>

    <div class="container">
        <div class="row align-center pdt-8 pdb-4">
            <div class="rounded text-center" >
                <a class="td-none c-yllw " href="<?php echo home_url()?>">  <img src="<?php  echo Utils::getAsset('images/arrow.svg')?>"></a>
            </div>
            <a class="td-none black ml-2 fw-6" href="<?php echo home_url()?>"> Terug naar projecten</a>
        </div>
        <div class="content">
            <div class="header">
                <h1 class="fw-6 fs-3"><?php echo $title?></h1>
            </div>
            <div class="post-info ml-8 mb-3">
                <ul>
                    <li><div class="row"><p class=" fw-6 mt-0">Lorem ipsum</p> <span class="ml-6 "><?php echo $title; ?></span></div></li>
                    <li><div class="row"><p class="fw-6 mt-0">Lorem ipsum</p> <span class="ml-6"><?php echo $year; ?></span></div></li>
                    <li><div class="row"><p class=" fw-6 mt-0">Lorem ipsum</p> <span class="ml-6"><?php echo $category; ?></span></div></li>
                    <li><div class="row"><p class=" fw-6 mt-0">Lorem ipsum</p> <span
                                    class="ml-6">Lorem ipsum dolor sit amet</span></div></li>
                    <li><div class="row"><p class=" fw-6 mt-0">Lorem ipsum</p> <span class="ml-6">Lorem ipsum dolor sit amet,consectetur adipiscing elit </span></div>
                    </li>

                </ul>
            </div>
            <div class="post-images ml-8 pd-h-lg-2">
                <?php
                Utils::renderTemplate('projecten-images');
                ?>

            </div>

        </div>
        <div class="footer row ml-8 pd-h-lg-2 mt-3" >
            <div class="prev-post col-50 row ">
                <div class="text-left  fw-5 ">
                    <?php previous_post_link( '%link', '<img src=" '.  Utils::getAsset('images/arrow2.svg') .'">', '' );  ?>
                </div>
                <div class="ml-1 fw-6 fss-6 lh-1 centered"><?php previous_post_link( '%link', '  %title project', '' );  ?></div>
            </div>
            <div class="next-post col-50 row  flex-end">
                <div class="mr-1 fw-6 fss-6 lh-1 centered"><?php next_post_link( '%link', '%title project', '' );  ?></div>
                <div class="text-right  fw-5">
                    <?php next_post_link( '%link', '<img src=" '.  Utils::getAsset('images/arrow3.svg') .'">', '' );  ?>
                </div>
            </div>
        </div>
        <div class="text-center pd-v-lg-4 ml-8">
            <a href="<?php echo home_url()?>#projecten" class="md-btn fs-1">Bekijk alle projecten</a>
        </div>
    </div>

<?php
get_footer();
