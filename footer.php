
<footer class="pd-1 bg-black text-center">
    <p class="m-0"><span>© 2021 Architectuurstudio Ina Jaspers.</span> Alle rechten voorbehouden. Website door <a href="https://www.andreapaolini.com/" target="_blank" class="white-link fw-5 td-none ">Andrea Paolini.</a></p>

</footer>

<?php wp_footer(); ?>
<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAqE1ow2LEKS7f7g7bZO6cRRhOtNAf9Z0&callback=initMap&v=weekly&channel=2"
        async
></script>
</body>
</html>