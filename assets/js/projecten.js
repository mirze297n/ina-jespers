jQuery(document).ready(function ($) {
    let ul = $('.projecten-list ul');
    let list = ul.children();
    let parContents = $('.projecten-images');
    let contents = parContents.children();
    $(document).on('click', '.projecten-list-item', function () {
        list.removeClass('active');
        $(this).addClass('active');
        let content = $(this).data('content');
        contents.removeClass('active-grid');
        $('.' + content).addClass('active-grid');
    });
});