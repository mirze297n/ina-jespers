jQuery(document).ready(function ($) {

    let slidesCont = $('.slides-container');
    let slideItems = $('.slide-items');
    let pause = 7000;
    let auto = false;
    let slides = slideItems.find('.slider-item');
    let pager = $('.pager');
    let slidesCount = slides.length;
    let currentSlide = slides.first();
    let currentSlideIndex = 0;
    let autoPlay = null;
    let arrowsCont = $('.arrows');
    let sliding = false;
    const slider_time = 500;


    slides.first().find('img').load(function () {
        let sliderHeight = slidesCont.height();
        slidesCont.height(sliderHeight);
        $('.slider-loader').hide();
    }).each(function () {
        if (this.complete) {
            $(this).trigger('load');
        }
    });

    if (slidesCount > 1) {
        auto = true;
    }
    let prevSlide = arrowsCont.children('.prev');
    let nextSlide = arrowsCont.children('.next');

    slides.not(':first').css('display', 'none');
    currentSlide.addClass('active');
    pager.children().eq(currentSlideIndex).addClass('active-dot');
    slides.each(function (index) {
        let active = '';
        if (index == 0) {
            active = 'active-dot';
        }
        pager.append('<span class="dot ' + active + '"></span>');
    });

    function fadeNext() {

        if (sliding) {
            return;
        }
        sliding = true;

        currentSlide.removeClass('active');
        pager.children().eq(currentSlideIndex).removeClass('active-dot');

        let newSlide, newSlideIndex;
        if (currentSlideIndex === slidesCount - 1) {
            newSlide = slides.first();
            newSlideIndex = 0;
        } else {
            newSlideIndex = currentSlideIndex + 1;
            newSlide = currentSlide.next();
        }

        pager.children().eq(newSlideIndex).addClass('active-dot');

        currentSlide.fadeOut(slider_time,
            function () {
                newSlide.addClass('active').fadeIn(slider_time, function () {
                    sliding = false;

                });
            });

        currentSlide = newSlide;
        currentSlideIndex = newSlideIndex;
    }

// Function responsible for fading to previous slide
    function fadePrev() {
        if (sliding) {
            return;
        }
        sliding = true;
        currentSlide.removeClass('active');
        pager.children().eq(currentSlideIndex).removeClass('active-dot');
        let newSlide, newSlideIndex;
        if (currentSlideIndex === 0) {
            newSlide = slides.last();
            newSlideIndex = slidesCount - 1;
        } else {
            newSlideIndex = currentSlideIndex - 1;
            newSlide = currentSlide.prev();
        }

        // pager.text(currentSlideIndex+' / '+slidesCount);
        pager.children().eq(newSlideIndex).addClass('active-dot');

        currentSlide.fadeOut(slider_time,
            function () {
                newSlide.addClass('active').fadeIn(slider_time, function () {
                    sliding = false;
                });
            });
        currentSlide = newSlide;
        currentSlideIndex = newSlideIndex;
    }

    function AutoPlay() {
        clearInterval(autoPlay);

        if (auto)
            autoPlay = setInterval(function () {
                if (document.hasFocus()) {
                    fadeNext()
                }
            }, pause);
    }

    $(nextSlide).click(function (e) {
        e.preventDefault();
        fadeNext();
        AutoPlay();
    });

// Detect if user clicked on arrow for previous slide and fade previous slide if it did
    $(prevSlide).click(function (e) {
        e.preventDefault();
        fadePrev();
        AutoPlay();
    });
    AutoPlay();
});
