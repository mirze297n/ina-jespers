<?php
use Eppeg\inajaspers\Utils;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <title>Architectuurstudio Ina Jaspers</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="icon" type="image/svg+xml" href="<?php echo Utils::getAsset('images/favicon.svg'); ?>" >
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="row align-center pd-h-lg-2">
    <div class="justify-sb text-center row align-center pd-v-lg-1">
        <a class="header-logo ml-1 td-none black" href= "<?php echo home_url();?>" ><img  src="<?php echo Utils::getAsset('images/aij-logo.svg'); ?>" alt="logo"></a>
    </div>

    <nav class="col-80 site-nav">
        <ul class="text-center full-width">
            <li class="pd-h-lg-2 inline-block"><a href="<?php echo site_url() ?>#projecten" class="pointer td-none black fw-8 fs-1 hvr-yllw to__target" data-target="#projecten">Projecten</a> </li>
            <li class="pd-h-lg-2 inline-block"><a href="<?php echo site_url() ?>#studio" class="pointer td-none  black fw-8 fs-1 hvr-yllw to__target" data-target="#studio">Studio</a> </li>
            <li class="pd-h-lg-2 inline-block"><a href="<?php echo site_url() ?>#contact" class="pointer td-none fw-8  black fs-1 hvr-yllw to__target" data-target="#contact">Contact</a> </li>
        </ul>
    </nav>
</header>