<?php

namespace Eppeg\inajaspers;

use WP_Query;
class App
{

    static function run()
    {
        add_action('init', ['Eppeg\inajaspers\App', 'create_slider_postType'], 0);
        add_action('init', ['Eppeg\inajaspers\App', 'create_projecten_postType']);
        add_action('init', ['Eppeg\inajaspers\App', 'projecten_taxonomy']);
        add_action('after_setup_theme', ['Eppeg\inajaspers\App', 'my_theme_setup']);
        add_action('wp_enqueue_scripts', ['Eppeg\inajaspers\App', 'enqueue_scripts_styles']);
    }

    static function enqueue_scripts_styles()
    {
        wp_enqueue_style('ina_public_style', Utils::getAsset('css/main.css'));
        wp_enqueue_script('ina_public_slider', Utils::getAsset('js/slider.js'), ['jquery']);
        wp_enqueue_script('ina_public_script', Utils::getAsset('js/front.js'), ['jquery']);
        wp_enqueue_script('ina_public_projecten', Utils::getAsset('js/projecten.js'), ['jquery']);
        wp_enqueue_script('lightbox_public_script', Utils::getAsset('js/lightbox-plus-jquery.js'), ['jquery']);
        wp_enqueue_style('lightbox_public_style', Utils::getAsset('css/lightbox.css'));
    }

    static function create_slider_postType()
    {

        $args = [
            'labels' => [
                'name' => __('Ina Slider'),
                'singular_name' => __('Ina Slider'),
                'add_new' => __('Add Slide', 'text-domain'),
                'edit_item' => __('Edit Slide', 'text-domain'),
                'new_item' => __('New Slide', 'text-domain'),
                'view_item' => __('View Slide', 'text-domain'),
                'not_found' => __('No Slide found', 'text-domain'),
                'not_found_in_trash' => __('No Slide found in trash', 'text-domain'),
            ],
            'supports' => ['title', 'editor', 'thumbnail'],
            'public' => true,
            'rewrite' => ['slug' => 'ina_slider'],
            'capability_type' => 'post',
        ];
        register_post_type('ina_slider', $args);
    }

    static function create_projecten_postType()
    {

        $args = [
            'labels' => [
                'name' => __('Ina Projecten'),
                'singular_name' => __('Ina Projecten'),
                'add_new' => __('Add Projecten', 'text-domain'),
                'edit_item' => __('Edit Projecten', 'text-domain'),
                'new_item' => __('New Projecten', 'text-domain'),
                'view_item' => __('View Projecten', 'text-domain'),
                'not_found' => __('No Projecten found', 'text-domain'),
                'not_found_in_trash' => __('No Projecten found in trash', 'text-domain'),
            ],
            'supports' => ['title', 'thumbnail'],
            'public' => true,
            'rewrite' => ['slug' => 'projecten'],
            'capability_type' => 'post',
        ];
        register_post_type('ina_projecten', $args);
    }

    static function projecten_taxonomy()
    {
        $args = [
            'hierarchical' => true,
            'label' => 'Projecten Categories', // display name
            'query_var' => true,
            'rewrite' => [
                'slug' => 'ina_projecten',    // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before
            ]
        ];
        register_taxonomy(
            'projecten_categories',  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'ina_projecten',             // post type name
            $args
        );
    }

    static function my_theme_setup()
    {
        add_theme_support('post-thumbnails');
    }

    static function getSliderFields()
    {
        $args = array(
            'post_type' => 'ina_slider',
            'post_status' => 'publish',
            'numberposts' => -1,
            'order' => 'ASC'
        );
        return get_posts($args);
    }

    static function getPostsbyCat($term)
    {
        $query = new WP_Query(['post_type' => 'ina_projecten',
            'tax_query' => [
                [
                    'taxonomy' => 'projecten_categories',
                    'field' => 'slug',
                    'terms' => $term,
                ]
            ],
        ]);
        return $query;
    }


}