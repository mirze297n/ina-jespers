<?php

namespace Eppeg\inajaspers;

class Utils
{
    static function renderTemplate( $name, $data = [], $once = false ) {
        $file = inajaspers_Template_Path . '/templates' . DIRECTORY_SEPARATOR . $name . '.php';
        if ( $once ) {
            require_once( $file );
        } else {
            require( $file );
        }
    }
    static function getTemplate( $name, $data = [], $once = false ) {
        ob_start();
        self::renderTemplate( $name, $data, $once );
        return ob_get_clean();
    }

    static function getAsset($name) {
        return inajaspers_Template_Url . '/assets/' . $name;
    }


}